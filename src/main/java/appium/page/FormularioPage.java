package appium.page;

import org.openqa.selenium.By;

import appium.core.BasePage;
import io.appium.java_client.MobileBy;
import junit.framework.Assert;

public class FormularioPage extends BasePage{
	

	
	public void escreverNome(String nome) {
		
		escrever(MobileBy.AccessibilityId("nome"), "Lucas");
		
	}

	public String obterNome() {
		return  obterTexto(MobileBy.AccessibilityId("nome"));
		  
	}
	
	public void selecionarCombo(String valor) {
		selecionarCombo(MobileBy.AccessibilityId("console"), valor);
	
	}
	
	public String obterValorDoCombo() {
		return obterTexto(By.xpath("//android.widget.Spinner/android.widget.TextView"));
		   
	}
	public void clicarCheck() {
		clicar(MobileBy.AccessibilityId("check"));
	}
	
	public void clicarSwitch() {
		clicar(MobileBy.AccessibilityId("switch"));
	}
	
	public void salvar() {
		clicarPorTexto("SALVAR");
	}
	
	public void salvarDemorado() {
		clicarPorTexto("SALVAR DEMORADO");
	}
	
	
	public boolean isCheckMarcado() {
		return isCheckMarcado(By.className("android.widget.CheckBox"));
	}
	
	public boolean isSwitchMarcado() {
		return isCheckMarcado(By.className("switch"));
	}
	

	public String obterNomeCadastrado() {
		return obterTexto(By.xpath("//android.widget.TextView[@text='Nome: Lucas']"));
	}
	public String obterConsoleCadastrado() {
		return obterTexto(By.xpath("//android.widget.TextView[@text='Console: ps4']"));
		
	}
	public String obterCheckCadastrado() {
		return obterTexto(By.xpath("//android.widget.TextView[@text='Checkbox: Marcado']"));

	}
	public String obterSwitchCadastrado() {
		return obterTexto(By.xpath("//android.widget.TextView[@text='Switch: Off']"));

	
	}
	
	
	
}
