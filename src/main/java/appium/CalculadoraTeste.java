package appium;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import appium.core.DriverFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class CalculadoraTeste {

	//Parametros Variaveis
	
	String nomeLivroQueroLer = "Em Chamas"; // trocar
	String nomeLivroParaHistorico = "Caixa de";
	String livro = "Em Chamas"; //trocar
	String nomePessoa = "Alessandra"; // trocar
	String adicionarNaMetaDeLeitura = "Morte no Nilo";
	String livroLido = "Eclipse";
	
	
	//@Test
	public void validarFalhaLoginComSenhaInvalida_001() throws MalformedURLException, InterruptedException {
	    DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	    desiredCapabilities.setCapability("platformName", "Android");
	    desiredCapabilities.setCapability("deviceName", "emulator-5554");
	    desiredCapabilities.setCapability("automationName", "uiautomator2");
	    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
	    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
	
	    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
	

	Thread.sleep(1000);  
	MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
	proximoButton.click();
	Thread.sleep(500); 
	MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton2.click();
	Thread.sleep(500); 
	MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton3.click();
	Thread.sleep(500); 
	MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton4.click();
	Thread.sleep(500); 
	MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton5.click();
	Thread.sleep(500); 
	MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton6.click();
	Thread.sleep(500);  
	
	MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
	fecharButton.click();
	Thread.sleep(500); 
	
	MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
	entrarButton.click();

	Thread.sleep(500); 
	
	MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
	emailButton.click();
	emailButton.sendKeys("lucas.cunha@base2.com.br");

	
	MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
	senhaButton.click();
	senhaButton.sendKeys("Testing2023");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
	
	
	MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
	entrarButton2.click();
	entrarButton2.click();

	
	Thread.sleep(500); 
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Email ou senha inválidos']")));

	
	String mensagemEsperada = "Email ou senha inválidos";
	String mensagemRecebida = driver.findElement(By.xpath("//android.widget.TextView[@text='Email ou senha inválidos']")).getText();
	Assert.assertEquals(mensagemRecebida, mensagemEsperada);

	driver.quit();
	
	    
	}
	
	//@Test
	public void sucessoLogin_002() throws MalformedURLException, InterruptedException {
	    DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	    desiredCapabilities.setCapability("platformName", "Android");
	    desiredCapabilities.setCapability("deviceName", "emulator-5554");
	    desiredCapabilities.setCapability("automationName", "uiautomator2");
	    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
	    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
	
	    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
	

	Thread.sleep(1000);  
	MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
	proximoButton.click();
	Thread.sleep(500); 
	MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton2.click();
	Thread.sleep(500); 
	MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton3.click();
	Thread.sleep(500); 
	MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton4.click();
	Thread.sleep(500); 
	MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton5.click();
	Thread.sleep(500); 
	MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton6.click();
	Thread.sleep(500);  
	
	MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
	fecharButton.click();
	Thread.sleep(500); 
	
	MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
	entrarButton.click();

	Thread.sleep(500); 
	
	MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
	emailButton.click();
	emailButton.sendKeys("lucas.cunha@base2.com.br");

	
	MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
	senhaButton.click();
	senhaButton.sendKeys("Testing2022");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
	
	
	MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
	entrarButton2.click();
	entrarButton2.click();

	

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Continuar']")));
	
	
	MobileElement continuarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Continuar']"));
	continuarButton.click();

	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[5]")));

	
	MobileElement perfilButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[5]"));
	perfilButton.click();

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Base2Test']")));

	
	String tituloEsperado = "Base2Test";
	String tituloLoginPerfil = driver.findElement(By.xpath("//android.widget.TextView[@text='Base2Test']")).getText();
	Assert.assertEquals(tituloLoginPerfil, tituloEsperado);
	
	



	driver.quit();
	
	}
	//@Test
	public void seguirUsuario_003() throws MalformedURLException, InterruptedException {
	    DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	    desiredCapabilities.setCapability("platformName", "Android");
	    desiredCapabilities.setCapability("deviceName", "emulator-5554");
	    desiredCapabilities.setCapability("automationName", "uiautomator2");
	    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
	    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
	
	    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
	

	Thread.sleep(1000);  
	MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
	proximoButton.click();
	Thread.sleep(500); 
	MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton2.click();
	Thread.sleep(500); 
	MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton3.click();
	Thread.sleep(500); 
	MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton4.click();
	Thread.sleep(500); 
	MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton5.click();
	Thread.sleep(500); 
	MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton6.click();
	Thread.sleep(500);  
	
	MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
	fecharButton.click();
	Thread.sleep(500); 
	
	MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
	entrarButton.click();

	Thread.sleep(500); 
	
	MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
	emailButton.click();
	emailButton.sendKeys("lucas.cunha@base2.com.br");

	
	MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
	senhaButton.click();
	senhaButton.sendKeys("Testing2022");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
	
	
	MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
	entrarButton2.click();
	entrarButton2.click();

	

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Continuar']")));
	
	
	MobileElement continuarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Continuar']"));
	continuarButton.click();

	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[5]")));
/*
	
	MobileElement perfilButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[5]"));
	perfilButton.click();

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Base2Test']")));

	
	String tituloEsperado = "Base2Test";
	String tituloLoginPerfil = driver.findElement(By.xpath("//android.widget.TextView[@text='Base2Test']")).getText();
	Assert.assertEquals(tituloLoginPerfil, tituloEsperado);
	
	*/
		MobileElement searchButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[3]"));
		searchButton.click();
		Thread.sleep(1000); 
		
		MobileElement filterButton = (MobileElement) driver.findElement(By.className("android.widget.EditText"));
		filterButton.click();
		filterButton.sendKeys("Lucas Ribeiro");
		Thread.sleep(1000); 

		MobileElement readersButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Leitores']"));
		readersButton.click();
		Thread.sleep(2000); 
		
		MobileElement randomUser = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'Lucas')]"));
		randomUser.click();
		Thread.sleep(2000); 
		
		MobileElement seguirButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Seguir']"));
		seguirButton.click();
		Thread.sleep(1000); 
		
		String textoEsperado = "Seguindo";
		String textoRecebido = driver.findElement(By.xpath("//android.widget.TextView[@text='Seguindo']")).getText();
		Assert.assertEquals(textoEsperado, textoRecebido);

		driver.quit();
		
	}

	//@Test
	public void pararDeSeguirUsuario_004() throws MalformedURLException, InterruptedException {
	    DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	    desiredCapabilities.setCapability("platformName", "Android");
	    desiredCapabilities.setCapability("deviceName", "emulator-5554");
	    desiredCapabilities.setCapability("automationName", "uiautomator2");
	    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
	    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
	
	    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
	

	Thread.sleep(1000);  
	MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
	proximoButton.click();
	Thread.sleep(500); 
	MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton2.click();
	Thread.sleep(500); 
	MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton3.click();
	Thread.sleep(500); 
	MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton4.click();
	Thread.sleep(500); 
	MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton5.click();
	Thread.sleep(500); 
	MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton6.click();
	Thread.sleep(500);  
	
	MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
	fecharButton.click();
	Thread.sleep(500); 
	
	MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
	entrarButton.click();

	Thread.sleep(500); 
	
	MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
	emailButton.click();
	emailButton.sendKeys("lucas.cunha@base2.com.br");

	
	MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
	senhaButton.click();
	senhaButton.sendKeys("Testing2022");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
	
	
	MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
	entrarButton2.click();
	entrarButton2.click();

	

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Continuar']")));
	
	
	MobileElement continuarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Continuar']"));
	continuarButton.click();

	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[5]")));

	
	MobileElement perfilButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[5]"));
	perfilButton.click();

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Base2Test']")));

	
	
		
		MobileElement seguindoButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Seguindo']"));
		seguindoButton.click();
		Thread.sleep(2000); 
		
		MobileElement firstFollowing = (MobileElement) driver.findElement(By.xpath("//android.widget.ScrollView//android.view.ViewGroup//android.view.ViewGroup//android.view.ViewGroup"));
		firstFollowing.click();
		Thread.sleep(1000); 
		
		MobileElement seguindoButton2 = (MobileElement) driver.findElement(By.xpath("(//android.widget.TextView[@text='Seguindo'])[2]"));
		seguindoButton2.click();
		Thread.sleep(1000); 
		
		
		String textoEsperado = "Seguir";
		String textoRecebido = driver.findElement(By.xpath("//android.widget.TextView[@text='Seguir']")).getText();
		Assert.assertEquals(textoEsperado, textoRecebido);
		
		driver.quit();
		
		
	}
	
	//@Test
	public void adicionarUmLivroNaMetaDeLeitura_005() throws InterruptedException, MalformedURLException {
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	    desiredCapabilities.setCapability("platformName", "Android");
	    desiredCapabilities.setCapability("deviceName", "emulator-5554");
	    desiredCapabilities.setCapability("automationName", "uiautomator2");
	    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
	    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
	
	    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
	

	Thread.sleep(1000);  
	MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
	proximoButton.click();
	Thread.sleep(500); 
	MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton2.click();
	Thread.sleep(500); 
	MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton3.click();
	Thread.sleep(500); 
	MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton4.click();
	Thread.sleep(500); 
	MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton5.click();
	Thread.sleep(500); 
	MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton6.click();
	Thread.sleep(500);  
	
	MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
	fecharButton.click();
	Thread.sleep(500); 
	
	MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
	entrarButton.click();

	Thread.sleep(500); 
	
	MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
	emailButton.click();
	emailButton.sendKeys("lucas.cunha@base2.com.br");

	
	MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
	senhaButton.click();
	senhaButton.sendKeys("Testing2022");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
	
	
	MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
	entrarButton2.click();
	entrarButton2.click();

	

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Continuar']")));
	
	
	MobileElement continuarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Continuar']"));
	continuarButton.click();

	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[5]")));

	MobileElement perfilButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[5]"));
	perfilButton.click();
	Thread.sleep(1000); 
	
	MobileElement goalProfile4Text = (MobileElement) driver.findElement(By.xpath("(//android.view.ViewGroup//android.view.ViewGroup//android.view.ViewGroup//android.widget.TextView)[15]"));
	System.out.println(goalProfile4Text.getText());
	String anterior = goalProfile4Text.getText();
	Thread.sleep(1000); 
	
	
		MobileElement searchButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[3]"));
		searchButton.click();
		Thread.sleep(1000); 
		
		MobileElement filterButton = (MobileElement) driver.findElement(By.className("android.widget.EditText"));
		filterButton.click();
		filterButton.sendKeys(adicionarNaMetaDeLeitura);
		Thread.sleep(2000); 
		
		
		MobileElement clickOnTheBook = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Lua Nova']"));
		clickOnTheBook.click();
		Thread.sleep(1000); 
		
		MobileElement plusButton = (MobileElement) driver.findElement(By.xpath("(//android.widget.TextView)[2]"));
		plusButton.click();
		Thread.sleep(1000); 
		MobileElement plusOptionsButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='MAIS OPÇÕES']"));
		plusOptionsButton.click();		Thread.sleep(1000); 
		
		
		MobileElement wantToReadButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Quero ler']"));
		wantToReadButton.click();		Thread.sleep(1000); 
		
		MobileElement wantToRead2022Button = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='2022']"));
		wantToRead2022Button.click();
		Thread.sleep(1000); 
		
		MobileElement xButton = (MobileElement) driver.findElement(By.xpath("(//android.view.ViewGroup//android.view.ViewGroup//android.view.ViewGroup//android.widget.TextView)[10]"));
		xButton.click();
		Thread.sleep(1000); 


	perfilButton.click();
	Thread.sleep(1000); 
	
	System.out.println(goalProfile4Text.getText());
	String atual = goalProfile4Text.getText();
	Thread.sleep(1000); 
	
	Assert.assertNotEquals(anterior, atual);
	
	 
	 

	driver.quit();
	
		
		
	}

	//@Test
	public void enviarMensagemParaUmSeguindo_006() throws InterruptedException, MalformedURLException {
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	    desiredCapabilities.setCapability("platformName", "Android");
	    desiredCapabilities.setCapability("deviceName", "emulator-5554");
	    desiredCapabilities.setCapability("automationName", "uiautomator2");
	    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
	    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
	
	    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
	

	Thread.sleep(1000);  
	MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
	proximoButton.click();
	Thread.sleep(500); 
	MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton2.click();
	Thread.sleep(500); 
	MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton3.click();
	Thread.sleep(500); 
	MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton4.click();
	Thread.sleep(500); 
	MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton5.click();
	Thread.sleep(500); 
	MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton6.click();
	Thread.sleep(500);  
	
	MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
	fecharButton.click();
	Thread.sleep(500); 
	
	MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
	entrarButton.click();

	Thread.sleep(500); 
	
	MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
	emailButton.click();
	emailButton.sendKeys("lucas.cunha@base2.com.br");

	
	MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
	senhaButton.click();
	senhaButton.sendKeys("Testing2022");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
	
	
	MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
	entrarButton2.click();
	entrarButton2.click();

	

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Continuar']")));
	
	
	MobileElement continuarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Continuar']"));
	continuarButton.click();

	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[5]")));

	
	MobileElement perfilButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[5]"));
	perfilButton.click();

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Base2Test']")));

		MobileElement seguindoButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Seguindo']"));
		seguindoButton.click();
		Thread.sleep(2000); 
		
		MobileElement firstFollowing = (MobileElement) driver.findElement(By.xpath("//android.widget.ScrollView//android.view.ViewGroup//android.view.ViewGroup//android.view.ViewGroup"));
		firstFollowing.click();
		Thread.sleep(1000); 
		
		MobileElement messageIcon = (MobileElement) driver.findElement(By.xpath("(//android.view.ViewGroup//android.view.ViewGroup//android.widget.TextView)[3]"));
		messageIcon.click();
		Thread.sleep(1000); 
		
		MobileElement insertMessageField = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Mensagem...']"));
		insertMessageField.click();
		insertMessageField.sendKeys("Ola, tudo bem?");
		
		MobileElement sendButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Enviar']"));
		sendButton.click();
		Thread.sleep(2000);
		String resultadoEsperado = "Ola, tudo bem?";
		
		MobileElement sentMessage = (MobileElement) driver.findElement(By.xpath("(//android.widget.TextView)[3]"));
		String resultadoRecebido = sentMessage.getText();
		System.out.println(driver.findElement(By.xpath("(//android.widget.TextView)[3]")).getText());

		
		Assert.assertEquals(resultadoEsperado,resultadoRecebido);
		
		// parar de seguir a pessoa 	
		

		driver.quit();
		
		
	}

	//@Test
	public void pesquisarUmAutor_007() throws InterruptedException, MalformedURLException {
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	    desiredCapabilities.setCapability("platformName", "Android");
	    desiredCapabilities.setCapability("deviceName", "emulator-5554");
	    desiredCapabilities.setCapability("automationName", "uiautomator2");
	    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
	    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
	
	    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
	

	Thread.sleep(1000);  
	MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
	proximoButton.click();
	Thread.sleep(500); 
	MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton2.click();
	Thread.sleep(500); 
	MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton3.click();
	Thread.sleep(500); 
	MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton4.click();
	Thread.sleep(500); 
	MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton5.click();
	Thread.sleep(500); 
	MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton6.click();
	Thread.sleep(500);  
	
	MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
	fecharButton.click();
	Thread.sleep(500); 
	
	MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
	entrarButton.click();

	Thread.sleep(500); 
	
	MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
	emailButton.click();
	emailButton.sendKeys("lucas.cunha@base2.com.br");

	
	MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
	senhaButton.click();
	senhaButton.sendKeys("Testing2022");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
	
	
	MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
	entrarButton2.click();
	entrarButton2.click();

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Continuar']")));
	
	
	MobileElement continuarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Continuar']"));
	continuarButton.click();

	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[5]")));

		MobileElement searchButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[3]"));
		searchButton.click();
		Thread.sleep(1000); 
		
		MobileElement filterButton = (MobileElement) driver.findElement(By.className("android.widget.EditText"));
		filterButton.click();
		String autor = "Suzanne Collins";
		filterButton.sendKeys(autor);
		Thread.sleep(3000); 
		
		//Verificar se foi encontrado pelo menos um livro do autor na pesquisa. 
	
		//System.out.println(driver.findElement(By.xpath("(//android.view.ViewGroup//android.view.ViewGroup//android.widget.TextView)[0]")).getText());

		System.out.println(driver.findElement(By.xpath("(//android.view.ViewGroup//android.view.ViewGroup//android.widget.TextView)[8]")).getText());
		String autorRecebido = driver.findElement(By.xpath("(//android.view.ViewGroup//android.view.ViewGroup//android.widget.TextView)[8]")).getText();
		System.out.println(driver.findElement(By.xpath("(//android.view.ViewGroup//android.view.ViewGroup//android.widget.TextView)[15]")).getText());
		
		Assert.assertEquals(autor, autorRecebido);
		
		

		driver.quit();
		
	}
	
	//@Test
	public void pesquisarUmLivro_008() throws InterruptedException, MalformedURLException {
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	    desiredCapabilities.setCapability("platformName", "Android");
	    desiredCapabilities.setCapability("deviceName", "emulator-5554");
	    desiredCapabilities.setCapability("automationName", "uiautomator2");
	    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
	    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
	
	    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
	

	Thread.sleep(1000);  
	MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
	proximoButton.click();
	Thread.sleep(500); 
	MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton2.click();
	Thread.sleep(500); 
	MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton3.click();
	Thread.sleep(500); 
	MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton4.click();
	Thread.sleep(500); 
	MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton5.click();
	Thread.sleep(500); 
	MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton6.click();
	Thread.sleep(500);  
	
	MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
	fecharButton.click();
	Thread.sleep(500); 
	
	MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
	entrarButton.click();

	Thread.sleep(500); 
	
	MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
	emailButton.click();
	emailButton.sendKeys("lucas.cunha@base2.com.br");

	
	MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
	senhaButton.click();
	senhaButton.sendKeys("Testing2022");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
	
	
	MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
	entrarButton2.click();
	entrarButton2.click();

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Continuar']")));
	
	
	MobileElement continuarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Continuar']"));
	continuarButton.click();

	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[5]")));

		MobileElement searchButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[3]"));
		searchButton.click();
		Thread.sleep(1000); 
		
		MobileElement filterButton = (MobileElement) driver.findElement(By.className("android.widget.EditText"));
		filterButton.click();
		String livro = "Mar de Monstros";
		filterButton.sendKeys(livro);
		Thread.sleep(3000); 
		
		//Verificar se foi encontrado pelo menos um livro do autor na pesquisa. 
	
		//System.out.println(driver.findElement(By.xpath("(//android.view.ViewGroup//android.view.ViewGroup//android.widget.TextView)[0]")).getText());

		System.out.println(driver.findElement(By.xpath("(//android.view.ViewGroup//android.view.ViewGroup//android.widget.TextView)[7]")).getText());
		String livroRecebido = driver.findElement(By.xpath("(//android.view.ViewGroup//android.view.ViewGroup//android.widget.TextView)[7]")).getText();

		Assert.assertTrue(livroRecebido.contains(livro));
		
		

		driver.quit();
		
	}
	
	//@Test
	public void adicionarUmLivroComoLido_009() throws InterruptedException, MalformedURLException {
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	    desiredCapabilities.setCapability("platformName", "Android");
	    desiredCapabilities.setCapability("deviceName", "emulator-5554");
	    desiredCapabilities.setCapability("automationName", "uiautomator2");
	    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
	    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
	
	    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
	

	Thread.sleep(1000);  
	MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
	proximoButton.click();
	Thread.sleep(500); 
	MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton2.click();
	Thread.sleep(500); 
	MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton3.click();
	Thread.sleep(500); 
	MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton4.click();
	Thread.sleep(500); 
	MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton5.click();
	Thread.sleep(500); 
	MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton6.click();
	Thread.sleep(500);  
	
	MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
	fecharButton.click();
	Thread.sleep(500); 
	
	MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
	entrarButton.click();

	Thread.sleep(500); 
	
	MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
	emailButton.click();
	emailButton.sendKeys("lucas.cunha@base2.com.br");

	
	MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
	senhaButton.click();
	senhaButton.sendKeys("Testing2022");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
	
	
	MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
	entrarButton2.click();
	entrarButton2.click();

	

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Continuar']")));
	
	
	MobileElement continuarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Continuar']"));
	continuarButton.click();

	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[5]")));

	MobileElement perfilButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[5]"));
	perfilButton.click();
	Thread.sleep(1000); 
	
	MobileElement goalProfile4Text = (MobileElement) driver.findElement(By.xpath("(//android.view.ViewGroup//android.view.ViewGroup//android.view.ViewGroup//android.widget.TextView)[15]"));
	System.out.println(goalProfile4Text.getText());
	String anterior = goalProfile4Text.getText();
	Thread.sleep(1000); 
	
	
		MobileElement searchButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[3]"));
		searchButton.click();
		Thread.sleep(1000); 
		
		MobileElement filterButton = (MobileElement) driver.findElement(By.className("android.widget.EditText"));
		filterButton.click();
		filterButton.sendKeys("Eclipse");
		Thread.sleep(4000); 
		
		
		MobileElement clickOnTheBook = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Eclipse ']"));
		clickOnTheBook.click();
		Thread.sleep(1000); 
		
		MobileElement plusButton = (MobileElement) driver.findElement(By.xpath("(//android.widget.TextView)[2]"));
		plusButton.click();
		Thread.sleep(1000); 
		
		MobileElement alreadyReadButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Já li']"));
		alreadyReadButton.click();		
		Thread.sleep(1000); 
		
		MobileElement howManyStarsQuestionText = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Quantas estrelas?']"));
		String resultadoRecebido = howManyStarsQuestionText.getText();		
		Thread.sleep(1000); 
		String resultadoEsperado = "Quantas estrelas?";
		Assert.assertEquals(resultadoEsperado, resultadoRecebido);


		driver.quit();
		
	}
	//@Test

	public void adicionarUmLivroComoLendo_010() throws InterruptedException, MalformedURLException {
			DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		    desiredCapabilities.setCapability("platformName", "Android");
		    desiredCapabilities.setCapability("deviceName", "emulator-5554");
		    desiredCapabilities.setCapability("automationName", "uiautomator2");
		    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
		    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
		
		    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
		

		Thread.sleep(1000);  
		MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
		proximoButton.click();
		Thread.sleep(1000); 
		MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
		proximoButton2.click();
		Thread.sleep(1000);  
		MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
		proximoButton3.click();
		Thread.sleep(500); 
		MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
		proximoButton4.click();
		Thread.sleep(500); 
		MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
		proximoButton5.click();
		Thread.sleep(500); 
		MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
		proximoButton6.click();
		Thread.sleep(500);  
		
		MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
		fecharButton.click();
		Thread.sleep(500); 
		
		MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
		entrarButton.click();

		Thread.sleep(500); 
		
		MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
		emailButton.click();
		emailButton.sendKeys("lucas.cunha@base2.com.br");

		
		MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
		senhaButton.click();
		senhaButton.sendKeys("Testing2022");
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
		
		
		MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
		entrarButton2.click();
		entrarButton2.click();

		

		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Continuar']")));
		
		
		MobileElement continuarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Continuar']"));
		continuarButton.click();

		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[5]")));

 
	
		
			MobileElement searchButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[3]"));
			searchButton.click();
			Thread.sleep(2000); 
			
			MobileElement filterButton = (MobileElement) driver.findElement(By.className("android.widget.EditText"));
			filterButton.click();
			filterButton.sendKeys("Eclipse");
			Thread.sleep(4000); 
			
			
			MobileElement clickOnTheBook = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Eclipse ']"));
			clickOnTheBook.click();
			Thread.sleep(2000); 
			
			MobileElement plusButton = (MobileElement) driver.findElement(By.xpath("(//android.widget.TextView)[2]"));
			plusButton.click();
			Thread.sleep(2000); 
			
			MobileElement alreadyReadButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Lendo']"));
			alreadyReadButton.click();		
			Thread.sleep(3000); 
			
			String resultadoRecebido = driver.findElement(By.xpath("(//android.widget.TextView[2])[1]")).getText();
		
			Thread.sleep(1000); 
			String resultadoEsperado = "Não deixe de voltar e dizer o que achou da leitura.";
			Assert.assertEquals(resultadoEsperado, resultadoRecebido);


			driver.quit();
				
		}

	//@Test
	public void adicionarUmLivroComoQueroLer_011() throws InterruptedException, MalformedURLException {
			DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		    desiredCapabilities.setCapability("platformName", "Android");
		    desiredCapabilities.setCapability("deviceName", "emulator-5554");
		    desiredCapabilities.setCapability("automationName", "uiautomator2");
		    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
		    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
		
		    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
		

		Thread.sleep(1000);  
		MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
		proximoButton.click();
		Thread.sleep(1000); 
		MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
		proximoButton2.click();
		Thread.sleep(1000);  
		MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
		proximoButton3.click();
		Thread.sleep(500); 
		MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
		proximoButton4.click();
		Thread.sleep(500); 
		MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
		proximoButton5.click();
		Thread.sleep(500); 
		MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
		proximoButton6.click();
		Thread.sleep(500);  
		
		MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
		fecharButton.click();
		Thread.sleep(500); 
		
		MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
		entrarButton.click();

		Thread.sleep(500); 
		
		MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
		emailButton.click();
		emailButton.sendKeys("lucas.cunha@base2.com.br");

		
		MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
		senhaButton.click();
		senhaButton.sendKeys("Testing2022");
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
		
		
		MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
		entrarButton2.click();
		entrarButton2.click();

		

		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Continuar']")));
		
		
		MobileElement continuarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Continuar']"));
		continuarButton.click();

		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[5]")));

 
	
		
			MobileElement searchButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[3]"));
			searchButton.click();
			Thread.sleep(2000); 
			
			MobileElement filterButton = (MobileElement) driver.findElement(By.className("android.widget.EditText"));
			filterButton.click();
			filterButton.sendKeys(livro);
			Thread.sleep(4000); 
			
			
			MobileElement clickOnTheBook = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='" + livro + "']"));
			clickOnTheBook.click();
			Thread.sleep(2000); 
			
			MobileElement plusButton = (MobileElement) driver.findElement(By.xpath("(//android.widget.TextView)[2]"));
			plusButton.click();
			Thread.sleep(2000); 
			
			MobileElement readButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Leitura']"));
			readButton.click();		
			Thread.sleep(3000); 
			
			MobileElement alreadyReadButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Quero ler']"));
			alreadyReadButton.click();		
			Thread.sleep(3000); 

			//System.out.println(driver.findElement(By.xpath("(//android.widget.TextView)[7]")).getText());

			
			String resultadoRecebido = driver.findElement(By.xpath("//android.widget.TextView[@text='Meta de Leitura']")).getText();
		
			Thread.sleep(1000); 
			String resultadoEsperado = "Meta de Leitura";
			Assert.assertEquals(resultadoEsperado, resultadoRecebido);

			
			MobileElement readButton1 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Já li']"));
			readButton1.click();		
			Thread.sleep(3000); 
			
			
			driver.quit();
			
		}

	//@Test
	public void validarQueUmLivroLendoNaoEPossivelSerResenhado_012() throws InterruptedException, MalformedURLException {
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	    desiredCapabilities.setCapability("platformName", "Android");
	    desiredCapabilities.setCapability("deviceName", "emulator-5554");
	    desiredCapabilities.setCapability("automationName", "uiautomator2");
	    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
	    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
	
	    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
	

	Thread.sleep(1000);  
	MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
	proximoButton.click();
	Thread.sleep(1000); 
	MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton2.click();
	Thread.sleep(1000);  
	MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton3.click();
	Thread.sleep(1000);  
	MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton4.click();
	Thread.sleep(1000);  
	MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton5.click();
	Thread.sleep(1000);  
	MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton6.click();
	Thread.sleep(1000);  
	
	MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
	fecharButton.click();
	Thread.sleep(1000);  
	
	MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
	entrarButton.click();

	Thread.sleep(1000);  
	
	MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
	emailButton.click();
	emailButton.sendKeys("lucas.cunha@base2.com.br");

	
	MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
	senhaButton.click();
	senhaButton.sendKeys("Testing2022");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
	
	
	MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
	entrarButton2.click();
	entrarButton2.click();

	

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Continuar']")));
	
	
	MobileElement continuarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Continuar']"));
	continuarButton.click();

	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[5]")));



	
		MobileElement searchButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[3]"));
		searchButton.click();
		Thread.sleep(2000); 
		
		MobileElement filterButton = (MobileElement) driver.findElement(By.className("android.widget.EditText"));
		filterButton.click();
		filterButton.sendKeys(livro);
		Thread.sleep(4000); 
		
		
		MobileElement clickOnTheBook = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='" + livro + "']"));
		clickOnTheBook.click();
		Thread.sleep(2000); 
		
		MobileElement plusButton = (MobileElement) driver.findElement(By.xpath("(//android.widget.TextView)[2]"));
		plusButton.click();
		Thread.sleep(2000); 
		
		MobileElement alreadyReadButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Lendo']"));
		alreadyReadButton.click();		
		Thread.sleep(1000); 
		
		//MobileElement plusOptionsButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='MAIS OPÇÕES']"));
		//plusOptionsButton.click();		
		Thread.sleep(1000); 
		
		MobileElement reviewButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Resenha']"));
		reviewButton.click();		
		Thread.sleep(1000); 
		

		System.out.println(driver.findElement(By.xpath("(//android.view.ViewGroup//android.widget.TextView[1])[11]")).getText());

		String resultadoRecebido = driver.findElement(By.xpath("(//android.view.ViewGroup//android.widget.TextView[1])[11]")).getText();
	
		Thread.sleep(1000); 
		String resultadoEsperado = "Para fazer a resenha, seu livro precisa estar marcado como LIDO.";
		Assert.assertEquals(resultadoEsperado, resultadoRecebido);

		
		MobileElement readButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Leitura']"));
		readButton.click();		
		Thread.sleep(3000); 
		
		MobileElement readButton1 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Já li']"));
		readButton1.click();		
		Thread.sleep(3000); 

		
		
		driver.quit();
		
	}
	
	//@Test
	public void acessarPaginaDeLancamentos_013() throws InterruptedException, MalformedURLException {

		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	    desiredCapabilities.setCapability("platformName", "Android");
	    desiredCapabilities.setCapability("deviceName", "emulator-5554");
	    desiredCapabilities.setCapability("automationName", "uiautomator2");
	    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
	    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
	
	    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
	

	Thread.sleep(1000);  
	MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
	proximoButton.click();
	Thread.sleep(1000); 
	MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton2.click();
	Thread.sleep(1000);  
	MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton3.click();
	Thread.sleep(1000);  
	MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton4.click();
	Thread.sleep(1000);  
	MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton5.click();
	Thread.sleep(1000);  
	MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton6.click();
	Thread.sleep(1000);  
	
	MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
	fecharButton.click();
	Thread.sleep(1000);  
	
	MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
	entrarButton.click();

	Thread.sleep(1000);  
	
	MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
	emailButton.click();
	emailButton.sendKeys("lucas.cunha@base2.com.br");

	
	MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
	senhaButton.click();
	senhaButton.sendKeys("Testing2022");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
	
	
	MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
	entrarButton2.click();
	entrarButton2.click();

	Thread.sleep(1000);

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Continuar']")));
	
	
	MobileElement continuarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Continuar']"));
	continuarButton.click();

	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[2]")));
	

	
	MobileElement newReleasesButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[2]"));
	newReleasesButton.click();

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Lançamentos']")));

	
	String tituloEsperadoTela = "Lançamentos";
	String tituloRecebidoTela = driver.findElement(By.xpath("//android.widget.TextView[@text='Lançamentos']")).getText();
	Assert.assertEquals(tituloEsperadoTela, tituloRecebidoTela);
	

	driver.quit();
	
	}

	//@Test
	public void validarFalhaLoginComEmailInvalido_014() throws MalformedURLException, InterruptedException {
	    DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	    desiredCapabilities.setCapability("platformName", "Android");
	    desiredCapabilities.setCapability("deviceName", "emulator-5554");
	    desiredCapabilities.setCapability("automationName", "uiautomator2");
	    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
	    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
	
	    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
	

	Thread.sleep(1000);  
	MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
	proximoButton.click();
	Thread.sleep(500); 
	MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton2.click();
	Thread.sleep(500); 
	MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton3.click();
	Thread.sleep(500); 
	MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton4.click();
	Thread.sleep(500); 
	MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton5.click();
	Thread.sleep(500); 
	MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton6.click();
	Thread.sleep(500);  
	
	MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
	fecharButton.click();
	Thread.sleep(500); 
	
	MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
	entrarButton.click();

	Thread.sleep(500); 
	
	MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
	emailButton.click();
	emailButton.sendKeys("lucas.cunXha@base2.com.br");

	
	MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
	senhaButton.click();
	senhaButton.sendKeys("Testing2022");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
	
	
	MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
	entrarButton2.click();
	entrarButton2.click();

	
	Thread.sleep(500); 
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Email ou senha inválidos']")));

	
	String mensagemEsperada = "Email ou senha inválidos";
	String mensagemRecebida = driver.findElement(By.xpath("//android.widget.TextView[@text='Email ou senha inválidos']")).getText();
	Assert.assertEquals(mensagemRecebida, mensagemEsperada);


	driver.quit();
	    
	}
	
	//@Test
	public void acessarPaginaDeCortesias_015() throws MalformedURLException, InterruptedException {

		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	    desiredCapabilities.setCapability("platformName", "Android");
	    desiredCapabilities.setCapability("deviceName", "emulator-5554");
	    desiredCapabilities.setCapability("automationName", "uiautomator2");
	    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
	    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
	
	    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
	

	Thread.sleep(1000);  
	MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
	proximoButton.click();
	Thread.sleep(1000); 
	MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton2.click();
	Thread.sleep(1000);  
	MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton3.click();
	Thread.sleep(1000);  
	MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton4.click();
	Thread.sleep(1000);  
	MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton5.click();
	Thread.sleep(1000);  
	MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton6.click();
	Thread.sleep(1000);  
	
	MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
	fecharButton.click();
	Thread.sleep(1000);  
	
	MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
	entrarButton.click();

	Thread.sleep(1000);  
	
	MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
	emailButton.click();
	emailButton.sendKeys("lucas.cunha@base2.com.br");

	
	MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
	senhaButton.click();
	senhaButton.sendKeys("Testing2022");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
	
	
	MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
	entrarButton2.click();
	entrarButton2.click();

	Thread.sleep(1000);

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Continuar']")));
	
	
	MobileElement continuarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Continuar']"));
	continuarButton.click();

	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[2]")));
	

	
	MobileElement newReleasesButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[2]"));
	newReleasesButton.click();

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Lançamentos']")));

	MobileElement cortesyButton = (MobileElement) driver.findElement(By.xpath("//android.view.ViewGroup[3]//android.view.ViewGroup"));
	cortesyButton.click();
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Cortesias']")));

	String tituloEsperadoTela = "Cortesias";
	String tituloRecebidoTela = driver.findElement(By.xpath("//android.widget.TextView[@text='Cortesias']")).getText();
	Assert.assertEquals(tituloEsperadoTela, tituloRecebidoTela);
	

	driver.quit();
		
	}
	
	//@Test
	public void acessarMeusLivros_016() throws InterruptedException, MalformedURLException {
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	    desiredCapabilities.setCapability("platformName", "Android");
	    desiredCapabilities.setCapability("deviceName", "emulator-5554");
	    desiredCapabilities.setCapability("automationName", "uiautomator2");
	    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
	    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
	
	    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
	

	Thread.sleep(1000);  
	MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
	proximoButton.click();
	Thread.sleep(500); 
	MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton2.click();
	Thread.sleep(500); 
	MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton3.click();
	Thread.sleep(500); 
	MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton4.click();
	Thread.sleep(500); 
	MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton5.click();
	Thread.sleep(500); 
	MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton6.click();
	Thread.sleep(500);  
	
	MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
	fecharButton.click();
	Thread.sleep(500); 
	
	MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
	entrarButton.click();

	Thread.sleep(500); 
	
	MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
	emailButton.click();
	emailButton.sendKeys("lucas.cunha@base2.com.br");

	
	MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
	senhaButton.click();
	senhaButton.sendKeys("Testing2022");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
	
	
	MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
	entrarButton2.click();
	entrarButton2.click();

	

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Continuar']")));
	
	
	MobileElement continuarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Continuar']"));
	continuarButton.click();

	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[4]")));

	
	MobileElement perfilButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[4]"));
	perfilButton.click();

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='PÁGINAS LIDAS']")));

	
	String tituloEsperado = "PÁGINAS LIDAS";
	String tituloLoginPerfil = driver.findElement(By.xpath("//android.widget.TextView[@text='PÁGINAS LIDAS']")).getText();
	Assert.assertEquals(tituloLoginPerfil, tituloEsperado);


	driver.quit();
	
	}
	
	//@Test
	public void enviarConviteDeAmizade_0017() throws InterruptedException, MalformedURLException {
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	    desiredCapabilities.setCapability("platformName", "Android");
	    desiredCapabilities.setCapability("deviceName", "emulator-5554");
	    desiredCapabilities.setCapability("automationName", "uiautomator2");
	    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
	    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
	
	    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
	

	Thread.sleep(1000);  
	MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
	proximoButton.click();
	Thread.sleep(500); 
	MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton2.click();
	Thread.sleep(500); 
	MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton3.click();
	Thread.sleep(500); 
	MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton4.click();
	Thread.sleep(500); 
	MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton5.click();
	Thread.sleep(500); 
	MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton6.click();
	Thread.sleep(500);  
	
	MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
	fecharButton.click();
	Thread.sleep(500); 
	
	MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
	entrarButton.click();

	Thread.sleep(500); 
	
	MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
	emailButton.click();
	emailButton.sendKeys("lucas.cunha@base2.com.br");

	
	MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
	senhaButton.click();
	senhaButton.sendKeys("Testing2022");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
	
	
	MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
	entrarButton2.click();
	entrarButton2.click();

	

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Continuar']")));
	
	
	MobileElement continuarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Continuar']"));
	continuarButton.click();

	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[5]")));

		MobileElement searchButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[3]"));
		searchButton.click();
		Thread.sleep(1000); 
		
		MobileElement filterButton = (MobileElement) driver.findElement(By.className("android.widget.EditText"));
		filterButton.click();

		filterButton.sendKeys(nomePessoa);
		Thread.sleep(2000); 

		MobileElement readersButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Leitores']"));
		readersButton.click();
		Thread.sleep(2000); 
		
		MobileElement randomUser = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + nomePessoa + "')]"));
		randomUser.click();
		Thread.sleep(2000); 
		
		MobileElement moreOptionsButton = (MobileElement) driver.findElement(By.xpath("//android.view.ViewGroup[2]//android.view.ViewGroup[2]//android.view.ViewGroup[2]//android.view.ViewGroup[2]"));
		moreOptionsButton.click();

		Thread.sleep(1000); 
		MobileElement addUser = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'Amigos?')]"));
		addUser.click();
		Thread.sleep(1000); 
		
		MobileElement yesButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'Sim')]"));
		yesButton.click();
		Thread.sleep(1000); 
		MobileElement messageText = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'Solicitação enviada.')]"));
		String mensagemRecebida = messageText.getText();
		Thread.sleep(1000); 
		String mensagemEsperada = "Solicitação enviada.";
		Assert.assertEquals(mensagemRecebida, mensagemEsperada);
		
			
		
		
		driver.quit();
	}

	//@Test
	public void desfazerConviteDeAmizade_0018() throws InterruptedException, MalformedURLException {
			DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		    desiredCapabilities.setCapability("platformName", "Android");
		    desiredCapabilities.setCapability("deviceName", "emulator-5554");
		    desiredCapabilities.setCapability("automationName", "uiautomator2");
		    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
		    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
		
		    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
		

		Thread.sleep(1000);  
		MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
		proximoButton.click();
		Thread.sleep(500); 
		MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
		proximoButton2.click();
		Thread.sleep(500); 
		MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
		proximoButton3.click();
		Thread.sleep(500); 
		MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
		proximoButton4.click();
		Thread.sleep(500); 
		MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
		proximoButton5.click();
		Thread.sleep(500); 
		MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
		proximoButton6.click();
		Thread.sleep(500);  
		
		MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
		fecharButton.click();
		Thread.sleep(500); 
		
		MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
		entrarButton.click();

		Thread.sleep(500); 
		
		MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
		emailButton.click();
		emailButton.sendKeys("lucas.cunha@base2.com.br");

		
		MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
		senhaButton.click();
		senhaButton.sendKeys("Testing2022");
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
		
		
		MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
		entrarButton2.click();
		entrarButton2.click();

		

		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Continuar']")));
		
		
		MobileElement continuarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Continuar']"));
		continuarButton.click();

		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[5]")));

			MobileElement searchButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[3]"));
			searchButton.click();
			Thread.sleep(1000); 
			
			MobileElement filterButton = (MobileElement) driver.findElement(By.className("android.widget.EditText"));
			filterButton.click();
			filterButton.sendKeys("Stefan");
			Thread.sleep(2000); 

			MobileElement readersButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Leitores']"));
			readersButton.click();
			Thread.sleep(2000); 
			
			MobileElement randomUser = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'Stefan')]"));
			randomUser.click();
			Thread.sleep(2000); 
			
			MobileElement moreOptionsButton = (MobileElement) driver.findElement(By.xpath("//android.view.ViewGroup[2]//android.view.ViewGroup[2]//android.view.ViewGroup[2]//android.view.ViewGroup[2]"));
			moreOptionsButton.click();

			Thread.sleep(1000); 
			
			
			MobileElement addUser = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'Amigos?')]"));
			addUser.click();
			Thread.sleep(1000); 
			
			MobileElement yesButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'Sim')]"));
			yesButton.click();
			Thread.sleep(2000); 
			

			String mensagemEsperada = "Amigos?";
			
			MobileElement cancelButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'Cancelar')]"));
			cancelButton.click();
			Thread.sleep(2000); 
			
			MobileElement addUser1 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'Amigos?')]"));
			
			String mensagemRecebida = addUser1.getText();
			
			Assert.assertEquals(mensagemRecebida, mensagemEsperada);

			Thread.sleep(4000); 

			
			
			
			
			
			driver.quit();
		}
	
	//@Test
	public void adicionarUmHistoricoDeLeitura_019() throws InterruptedException, MalformedURLException {
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	    desiredCapabilities.setCapability("platformName", "Android");
	    desiredCapabilities.setCapability("deviceName", "emulator-5554");
	    desiredCapabilities.setCapability("automationName", "uiautomator2");
	    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
	    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
	
	    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
	

	Thread.sleep(1000);  
	MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
	proximoButton.click();
	Thread.sleep(1000); 
	MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton2.click();
	Thread.sleep(1000);  
	MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton3.click();
	Thread.sleep(500); 
	MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton4.click();
	Thread.sleep(500); 
	MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton5.click();
	Thread.sleep(500); 
	MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton6.click();
	Thread.sleep(500);  
	
	MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
	fecharButton.click();
	Thread.sleep(500); 
	
	MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
	entrarButton.click();

	Thread.sleep(500); 
	
	MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
	emailButton.click();
	emailButton.sendKeys("lucas.cunha@base2.com.br");

	
	MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
	senhaButton.click();
	senhaButton.sendKeys("Testing2022");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
	
	
	MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
	entrarButton2.click();
	entrarButton2.click();

	

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Continuar']")));
	
	
	MobileElement continuarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Continuar']"));
	continuarButton.click();

	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[5]")));



	
		MobileElement searchButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[3]"));
		searchButton.click();
		Thread.sleep(2000); 
		
		MobileElement filterButton = (MobileElement) driver.findElement(By.className("android.widget.EditText"));
		filterButton.click();
		String nomeLivro1 = "Jogos Vorazes";
		filterButton.sendKeys(nomeLivro1);
		Thread.sleep(4000); 
		
		
		MobileElement clickOnTheBook = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Jogos Vorazes']"));
		clickOnTheBook.click();
		Thread.sleep(2000); 
		
		MobileElement plusButton = (MobileElement) driver.findElement(By.xpath("(//android.widget.TextView)[2]"));
		plusButton.click();
		Thread.sleep(2000); 
		



		
		MobileElement newButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Novo']"));
		newButton.click();		
		Thread.sleep(3000); 
		
		
		MobileElement pageField = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[contains(@text,'lidas')]"));
		pageField.click();
		String paginasLidas = "72";
		pageField.sendKeys(paginasLidas);
		Thread.sleep(1000); 
		
		
		MobileElement pageCheckbox = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Páginas']"));
		pageCheckbox.click();	
		pageCheckbox.click();
		Thread.sleep(2000); 
		
		MobileElement saveButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Salvar']"));
		saveButton.click();	

		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[contains(@text,'sucesso')]")));
		
		MobileElement successMessage = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'sucesso')]"));
		String resultadoRecebido = successMessage.getText();	
		
		
		String resultadoEsperado = "Sua anotação foi salva com sucesso!";
		
		Assert.assertEquals(resultadoEsperado, resultadoRecebido);
		Thread.sleep(2000); 
		driver.quit();
		
	}

	//@Test
	public void avaliarUmLivroComoMuitoBom_020() throws InterruptedException, MalformedURLException {
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	    desiredCapabilities.setCapability("platformName", "Android");
	    desiredCapabilities.setCapability("deviceName", "emulator-5554");
	    desiredCapabilities.setCapability("automationName", "uiautomator2");
	    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
	    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
	
	    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
	

	Thread.sleep(1000);  
	MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
	proximoButton.click();
	Thread.sleep(500); 
	MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton2.click();
	Thread.sleep(500); 
	MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton3.click();
	Thread.sleep(500); 
	MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton4.click();
	Thread.sleep(500); 
	MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton5.click();
	Thread.sleep(500); 
	MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton6.click();
	Thread.sleep(500);  
	
	MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
	fecharButton.click();
	Thread.sleep(500); 
	
	MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
	entrarButton.click();

	Thread.sleep(500); 
	
	MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
	emailButton.click();
	emailButton.sendKeys("lucas.cunha@base2.com.br");

	
	MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
	senhaButton.click();
	senhaButton.sendKeys("Testing2022");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
	
	
	MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
	entrarButton2.click();
	entrarButton2.click();

	

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Continuar']")));
	
	
	MobileElement continuarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Continuar']"));
	continuarButton.click();

	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[5]")));


	
	
		MobileElement searchButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[3]"));
		searchButton.click();
		Thread.sleep(1000); 
		
		MobileElement filterButton = (MobileElement) driver.findElement(By.className("android.widget.EditText"));
		filterButton.click();
		filterButton.sendKeys("Eclipse");
		Thread.sleep(4000); 
		
		
		MobileElement clickOnTheBook = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Eclipse ']"));
		clickOnTheBook.click();
		Thread.sleep(1000); 
		
		MobileElement plusButton = (MobileElement) driver.findElement(By.xpath("(//android.widget.TextView)[2]"));
		plusButton.click();
		Thread.sleep(1000); 
		
		MobileElement alreadyReadButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Já li']"));
		alreadyReadButton.click();		
		Thread.sleep(1000); 

		
		MobileElement veryGoodButton = (MobileElement) driver.findElement(By.xpath("//android.view.ViewGroup//android.widget.Button[5]"));
		veryGoodButton.click();		
		Thread.sleep(1000); 
		
		MobileElement veryGoodText = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='MUITO BOM']"));
		String resultadoRecebido = veryGoodText.getText();	
		String resultadoEsperado = "MUITO BOM";
		Assert.assertEquals(resultadoRecebido, resultadoEsperado);
		Thread.sleep(1000); 


		driver.quit();
			
	}
	//@Test
	public void avaliarUmLivroComoRuim_021() throws InterruptedException, MalformedURLException {
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	    desiredCapabilities.setCapability("platformName", "Android");
	    desiredCapabilities.setCapability("deviceName", "emulator-5554");
	    desiredCapabilities.setCapability("automationName", "uiautomator2");
	    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
	    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
	
	    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
	

	Thread.sleep(1000);  
	MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
	proximoButton.click();
	Thread.sleep(500); 
	MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton2.click();
	Thread.sleep(500); 
	MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton3.click();
	Thread.sleep(500); 
	MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton4.click();
	Thread.sleep(500); 
	MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton5.click();
	Thread.sleep(500); 
	MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton6.click();
	Thread.sleep(500);  
	
	MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
	fecharButton.click();
	Thread.sleep(500); 
	
	MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
	entrarButton.click();

	Thread.sleep(500); 
	
	MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
	emailButton.click();
	emailButton.sendKeys("lucas.cunha@base2.com.br");

	
	MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
	senhaButton.click();
	senhaButton.sendKeys("Testing2022");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
	
	
	MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
	entrarButton2.click();
	entrarButton2.click();

	

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Continuar']")));
	
	
	MobileElement continuarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Continuar']"));
	continuarButton.click();

	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[5]")));


	
	
		MobileElement searchButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[3]"));
		searchButton.click();
		Thread.sleep(1000); 
		
		MobileElement filterButton = (MobileElement) driver.findElement(By.className("android.widget.EditText"));
		filterButton.click();
		String livro = "Jogos Vorazes";
		filterButton.sendKeys(livro);
		Thread.sleep(4000); 
		
		
		MobileElement clickOnTheBook = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Jogos Vorazes']"));
		clickOnTheBook.click();
		Thread.sleep(1000); 
		
		MobileElement plusButton = (MobileElement) driver.findElement(By.xpath("(//android.widget.TextView)[2]"));
		plusButton.click();
		Thread.sleep(1000); 
		
		MobileElement alreadyReadButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Já li']"));
		alreadyReadButton.click();		
		Thread.sleep(1000); 

		
		MobileElement badButton = (MobileElement) driver.findElement(By.xpath("//android.view.ViewGroup//android.widget.Button[2]"));
		badButton.click();		
		Thread.sleep(1000); 
		
		MobileElement badText = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='RUIM']"));
		String resultadoRecebido = badText.getText();	
		String resultadoEsperado = "RUIM";
		Assert.assertEquals(resultadoRecebido, resultadoEsperado);
		Thread.sleep(1000); 

		MobileElement plus1Button = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='MAIS OPÇÕES']"));
		plus1Button.click();
		Thread.sleep(2000); 


		
		MobileElement deleteButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Excluir da estante']"));
		deleteButton.click();		
		Thread.sleep(2000); 
		
		driver.quit();
		
	}
	
	//@Test
	public void excluirUmLivroDaEstante_022() throws InterruptedException, MalformedURLException {
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	    desiredCapabilities.setCapability("platformName", "Android");
	    desiredCapabilities.setCapability("deviceName", "emulator-5554");
	    desiredCapabilities.setCapability("automationName", "uiautomator2");
	    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
	    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
	
	    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
	

	Thread.sleep(1000);  
	MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
	proximoButton.click();
	Thread.sleep(500); 
	MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton2.click();
	Thread.sleep(500); 
	MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton3.click();
	Thread.sleep(500); 
	MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton4.click();
	Thread.sleep(500); 
	MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton5.click();
	Thread.sleep(500); 
	MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton6.click();
	Thread.sleep(500);  
	
	MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
	fecharButton.click();
	Thread.sleep(500); 
	
	MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
	entrarButton.click();

	Thread.sleep(500); 
	
	MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
	emailButton.click();
	emailButton.sendKeys("lucas.cunha@base2.com.br");

	
	MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
	senhaButton.click();
	senhaButton.sendKeys("Testing2022");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
	
	
	MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
	entrarButton2.click();
	entrarButton2.click();

	

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Continuar']")));
	
	
	MobileElement continuarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Continuar']"));
	continuarButton.click();

	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[4]")));

	
	MobileElement perfilButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[4]"));
	perfilButton.click();

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='PÁGINAS LIDAS']")));

	
	String tituloEsperado = "PÁGINAS LIDAS";
	String tituloLoginPerfil = driver.findElement(By.xpath("//android.widget.TextView[@text='PÁGINAS LIDAS']")).getText();
	Assert.assertEquals(tituloLoginPerfil, tituloEsperado);

	MobileElement firstBookImage = (MobileElement) driver.findElement(By.xpath("(//android.view.ViewGroup//android.widget.ImageView)[3]"));
	firstBookImage.click();
	Thread.sleep(1000); 
	
	
	MobileElement plusButton = (MobileElement) driver.findElement(By.xpath("(//android.widget.TextView)[2]"));
	plusButton.click();
	Thread.sleep(2000); 


	
	MobileElement deleteButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Excluir da estante']"));
	deleteButton.click();		

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Retirado da estante!']")));
	
	
	MobileElement messageText = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Retirado da estante!']"));
	String mensagemRecebida = messageText.getText();		

	
	String resultadoEsperado = "Retirado da estante!";
	
	Assert.assertEquals(resultadoEsperado, mensagemRecebida);
	Thread.sleep(2000); 
	driver.quit();
	
	
	
		
	}
	
	//@Test
	public void visualizarMinhasRevistas_023() throws MalformedURLException, InterruptedException {
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	    desiredCapabilities.setCapability("platformName", "Android");
	    desiredCapabilities.setCapability("deviceName", "emulator-5554");
	    desiredCapabilities.setCapability("automationName", "uiautomator2");
	    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
	    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
	
	    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
	

	Thread.sleep(1000);  
	MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
	proximoButton.click();
	Thread.sleep(500); 
	MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton2.click();
	Thread.sleep(500); 
	MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton3.click();
	Thread.sleep(500); 
	MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton4.click();
	Thread.sleep(500); 
	MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton5.click();
	Thread.sleep(500); 
	MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton6.click();
	Thread.sleep(500);  
	
	MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
	fecharButton.click();
	Thread.sleep(500); 
	
	MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
	entrarButton.click();

	Thread.sleep(500); 
	
	MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
	emailButton.click();
	emailButton.sendKeys("lucas.cunha@base2.com.br");

	
	MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
	senhaButton.click();
	senhaButton.sendKeys("Testing2022");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
	
	
	MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
	entrarButton2.click();
	entrarButton2.click();

	Thread.sleep(500); 

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Continuar']")));
	
	
	MobileElement continuarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Continuar']"));
	continuarButton.click();
	Thread.sleep(500); 
	

	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[4]")));

	
	MobileElement perfilButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[4]"));
	perfilButton.click();
	Thread.sleep(1000); 
	

	MobileElement moreOptions2Button = (MobileElement) driver.findElement(By.xpath("(//android.view.ViewGroup//android.view.ViewGroup//android.view.ViewGroup[2]//android.widget.TextView)[2]"));
	moreOptions2Button.click();

	Thread.sleep(500); 
	
	
	MobileElement revistasButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Revistas']"));
	revistasButton.click();

	Thread.sleep(500); 
	
	MobileElement closeButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
	closeButton.click();

	Thread.sleep(500); 
	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Minhas']")));
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Revistas']")));

	Assert.assertEquals(driver.findElement(By.xpath("//android.widget.TextView[@text='Revistas']")).getText(), "Revistas");
	
	
	driver.quit();
	
	
	}
	//@Test
	public void visualizarMinhasHQ_024() throws MalformedURLException, InterruptedException {
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	    desiredCapabilities.setCapability("platformName", "Android");
	    desiredCapabilities.setCapability("deviceName", "emulator-5554");
	    desiredCapabilities.setCapability("automationName", "uiautomator2");
	    desiredCapabilities.setCapability("appPackage", "com.gaudium.skoob");
	    desiredCapabilities.setCapability("appActivity", "com.gaudium.skoob.MainActivity");
	
	    AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
	

	Thread.sleep(1000);  
	MobileElement proximoButton = (MobileElement) driver.findElement(By.xpath("//*[@text='Próximo']"));
	proximoButton.click();
	Thread.sleep(500); 
	MobileElement proximoButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton2.click();
	Thread.sleep(500); 
	MobileElement proximoButton3 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton3.click();
	Thread.sleep(500); 
	MobileElement proximoButton4 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton4.click();
	Thread.sleep(500); 
	MobileElement proximoButton5 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton5.click();
	Thread.sleep(500); 
	MobileElement proximoButton6 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Próximo']"));
	proximoButton6.click();
	Thread.sleep(500);  
	
	MobileElement fecharButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
	fecharButton.click();
	Thread.sleep(500); 
	
	MobileElement entrarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text=' Entrar ']"));
	entrarButton.click();

	Thread.sleep(500); 
	
	MobileElement emailButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Email']"));
	emailButton.click();
	emailButton.sendKeys("lucas.cunha@base2.com.br");

	
	MobileElement senhaButton = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@text='Senha']"));
	senhaButton.click();
	senhaButton.sendKeys("Testing2022");
	
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='ENTRAR']")));
	
	
	MobileElement entrarButton2 = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='ENTRAR']"));
	entrarButton2.click();
	entrarButton2.click();

	Thread.sleep(500); 

	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Continuar']")));
	
	
	MobileElement continuarButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Continuar']"));
	continuarButton.click();
	Thread.sleep(500); 
	

	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[4]")));

	
	MobileElement perfilButton = (MobileElement) driver.findElement(By.xpath("//android.widget.Button[4]"));
	perfilButton.click();
	Thread.sleep(1000); 
	

	MobileElement moreOptions2Button = (MobileElement) driver.findElement(By.xpath("(//android.view.ViewGroup//android.view.ViewGroup//android.view.ViewGroup[2]//android.widget.TextView)[2]"));
	moreOptions2Button.click();

	Thread.sleep(500); 
	
	
	MobileElement revistasButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='HQs (Quadrinhos)']"));
	revistasButton.click();

	Thread.sleep(500); 
	
	MobileElement closeButton = (MobileElement) driver.findElement(By.xpath("//android.widget.TextView[@text='Fechar']"));
	closeButton.click();

	Thread.sleep(500); 
	
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Minhas']")));
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='HQs']")));

	Assert.assertEquals(driver.findElement(By.xpath("//android.widget.TextView[@text='HQs']")).getText(), "HQs");
	
	
	driver.quit();
	
	
	}
	
	
}
