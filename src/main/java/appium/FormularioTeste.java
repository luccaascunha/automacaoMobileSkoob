package appium;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import appium.core.BaseTest;
import appium.core.DriverFactory;
import appium.page.FormularioPage;
import appium.page.MenuPage;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import junit.framework.Assert;

public class FormularioTeste extends BaseTest {
	
	private AndroidDriver<MobileElement> driver;
	
	
	private MenuPage menu = new MenuPage();
	private FormularioPage page = new FormularioPage();
	
	@Before
	public void inicializarAppium() throws MalformedURLException {
		
		driver = DriverFactory.getDriver();
		menu.acessarFormulario();
		
	}
	

	//Para entrar no emulador: entrar no cmd, cd %Android_Home%, cd tools, emulator @Nexus_5x_API_24
	// uiautomatorviewer
	//adb reconnect
	
	
	
	//@Test
	public void devePreencherCampoTexto() throws MalformedURLException {	   	    

		page.escreverNome("Lucas");
	    assertEquals("Lucas", page.obterNome());   
	    
	}
	
	//@Test
		public void deveInteragirComOCombo() throws MalformedURLException {

			page.selecionarCombo("PS4");
			assertEquals("PS4", page.obterValorDoCombo());
			
		}

		//@Test
		//public void deveInteragirSwitchCheckBox() throws MalformedURLException {

			
			//Verificar Status dos Elementos
			//Assert.assertFalse(dsl.isCheckMarcado(By.className("android.widget.CheckBox")));
		//	Assert.assertTrue(dsl.isCheckMarcado(MobileBy.AccessibilityId("switch")));
			
			//clicar nos elementos
			
		//	page.clicarCheck();
		//	page.clicarSwitch();
			
			//verificar status dos elementos
		//	Assert.assertTrue(dsl.isCheckMarcado(By.className("android.widget.CheckBox")));
		//	Assert.assertFalse(dsl.isCheckMarcado(MobileBy.AccessibilityId("switch")));
			
		
				    
		//	}
		
		@SuppressWarnings("deprecation")
		//@Test
		public void DesafioDemorado() throws MalformedURLException, InterruptedException {
			
			page.escreverNome("Lucas");
			page.clicarCheck();
			page.clicarSwitch();
			page.selecionarCombo("PS4");
			page.salvar();
			

			List<MobileElement> resultado = driver.findElementsByClassName("android.widget.TextView");
			for(MobileElement elemento: resultado) {
		    	//System.out.println(elemento.getText());
		    }
			
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			assertEquals("Nome: Lucas", page.obterNomeCadastrado());
			assertEquals("Console: ps4", page.obterConsoleCadastrado());
			assertEquals("Checkbox: Marcado", page.obterCheckCadastrado());
			assertEquals("Switch: Off", page.obterSwitchCadastrado());
		}

		@SuppressWarnings("deprecation")
		@Test
		public void DesafioRapido() throws MalformedURLException, InterruptedException {
			page.escreverNome("Lucas");
			driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
			
			page.salvarDemorado();
			
			//esperar(3000);
			WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(),10);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Nome: Lucas']")));
			
			List<MobileElement> resultado = driver.findElementsByClassName("android.widget.TextView");
			for(MobileElement elemento: resultado) {
		    	//System.out.println(elemento.getText());
		    }
			
			assertEquals("Nome: Lucas", page.obterNomeCadastrado());
		}
		

}
